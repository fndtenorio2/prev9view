<?php

/* Serializa arquivo com lista de cidades
$json = json_decode(file_get_contents("js/lista_cidades.js"));
$cidades = $json->cidades;

$map = array();

foreach ($cidades as $cidade) {
	$tokens = explode("-", $cidade->cidade);
	$map["$cidade->cidade"] = $cidade->cidcompleta."-".$tokens[1];
}

$obj_str = serialize($map);
file_put_contents("lista_cidades.obj", $obj_str);
*/

function normalize ($string)
{
    $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', ' ' =>  '', "'" => ''
    );
   
    return strtr(strtolower($string), $table);
 }

$query = normalize($_GET["query"]);
$lista = unserialize(file_get_contents("lista_cidades.obj"));

$cidades_data = array_slice(preg_grep("/^$query/i", array_keys($lista)), 0, 8);
$results = array();

foreach($cidades_data as $cid_data) 
	$results[] = array("value"=>$lista[$cid_data], "data"=>$cid_data);

$json = array("query"=>$query, "suggestions"=>$results);
header("Content-type: text/json");
echo json_encode($json);

?>


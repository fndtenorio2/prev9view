<?php

$cidade = $_GET['Cidade'];
$data_ini = $_GET["DtIni"];
$data_fim = $_GET["DtFim"];

$url_prev = "http://ws.somarmeteorologia.com.br/previsao9meses.php?Cidade=$cidade&DtIni=$data_ini&DtFim=$data_fim&type=json";
$url_clim = "http://ws.somarmeteorologia.com.br/climatologia.php?Cidade=$cidade";

$json_prev = json_decode(file_get_contents($url_prev));
$json_clim = simplexml_load_file($url_clim);

$data = array("prev"=>$json_prev, "clim"=>$json_clim);
header("Content-type: text/json");
echo json_encode($data, JSON_NUMERIC_CHECK);

?>